<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 */
function foundationpress_customizer( $wp_customize ) {

	$wp_customize->add_section( 'hero', array(
		'title'       => __( 'Hero Text', 'foundationpress' ),
		'priority'    => 1,
		'description' => __( 'Hero Text' ) // optional
	) );

	function customizer_textarea_sanitizer( $text ) {
		return esc_textarea( $text );
	}

	$wp_customize->add_setting( 'hero_text', array(
		'default'   => __( 'Hero Text Here', 'foundationpress' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_textarea_sanitizer',
	) );

	$wp_customize->add_control( 'hero_text', array(
		'label'    => __( 'Hero Text', 'foundationpress' ),
		'section'  => 'hero',
		'settings' => 'hero_text',
		'type'     => 'textarea',
	) );
}

add_action( 'customize_register', 'foundationpress_customizer' );