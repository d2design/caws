<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since   FoundationPress 1.0.0
 */
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
<?php endif; ?>

	<div class="top-bar-container" data-sticky-container>
		<div class="title-bar-container sticky" data-sticky
			 data-options="anchor: anchor-top; marginTop: 0; stickyOn: large;" style="width:100%; z-index:5">
			<!-- NOTE: This is the header menu that appears at the top of your site. -->
			<div class="off-canvas-content" data-off-canvas-content>
				<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>

					<div class="title-bar-left">
						<span class="site-mobile-title title-bar-title">
							<?php get_template_part( 'template-parts/navbar-logo' ) ?>
						</span>
					</div>
					<div class="title-bar-right">
						<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon"
								type="button"
								data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
					</div>
				</div>

				<nav class="site-navigation top-bar" role="navigation">
					<div class="top-bar-left">
						<div class="site-desktop-title top-bar-title">
							<?php get_template_part( 'template-parts/navbar-logo' ) ?>
						</div>
					</div>
					<div class="top-bar-right">
						<div class="site-desktop-title top-bar-title">
							<?php dynamic_sidebar( 'navbar-widget' ); ?>
						</div>
					</div>
				</nav>

				<div class="top-bar-right background-white">
					<?php foundationpress_top_bar_r(); ?>

				</div>
				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>


<header class="site-header" role="banner">

<?php get_template_part( 'template-parts/header/hero' ); ?>
</header>


