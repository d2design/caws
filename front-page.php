<?php
/*
Template Name: Front Page
*/
get_header();

do_action( 'before_after_content' );

//About section using wp_query
get_template_part( 'template-parts/front-page-sections/about' );

//Support section using wp_query
get_template_part( 'template-parts/front-page-sections/support' );

do_action( 'foundationpress_after_content' );

get_footer();
