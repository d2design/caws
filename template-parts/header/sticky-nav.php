<?php
/**
 * Sticky Nav
 */
?>

<div data-sticky-container>
	<div class="title-bar" data-sticky data-options="marginTop:0;" style="width:100%">
		<div class="title-bar-left">
			<?php get_template_part( 'template-parts/navbar-logo' ); ?>
		</div>
		<?php dynamic_sidebar( 'navbar-widget' ); ?>
	</div>
</div>