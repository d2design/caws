<?php

/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 2/24/18
 * Time: 3:13 PM
 */
?>

<div class="hero-content">
	<?php if ( is_front_page() ) { ?>
		<div class="overlay">
			<h4 class="hero__text--white">
				<?php echo get_theme_mod( 'hero_text' ); ?>
			</h4>
        </div>
	<?php }
	if ( is_front_page() ) {
		the_custom_header_markup();

	}
	?>
</div>

