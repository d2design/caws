<?php
/**
 * About Front Page Section
 */


$args = array(
	'post_type'      => 'page',
	'category_name'  => 'about',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
);

// Query for the about page
$about_query = new WP_Query( $args );

// "loop" through query (even though it's just one page)
if ( $about_query->have_posts() ) : $about_query->the_post(); ?>
	<section class="services mt-80">
		<div class="grid-container fluid">

			<div class="grid-x grid-margin-x align-center">
				<div class="cell small-12 large-6">
					<?php the_title( '<h2>', '</h2>' ); ?>
					<?php the_excerpt(); ?>
				</div>
				<div class="cell small-12 large-4">
					<div class="media-object-section">
						<div class="thumbnail">
							<?php the_post_thumbnail( 'featured-square' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php wp_reset_postdata(); ?>