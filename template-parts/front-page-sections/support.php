<?php
/**
 * About Front Page Section
 */
?>

	<section class="services mt-80">
		<div class="grid-container">
			<h2 class="text-center mb-20">Support Patches</h2>
		</div>

		<?php
	echo '<div class="grid-x grid-padding-x small-up-1 large-up-3 align-center-middle text-center">';

		$i = 1; // Count to return a new 4-col row

		$support_args = array(
			'post_type'     => 'page',
			'category_name' => 'support',
		);

$the_support_query = new WP_Query( $support_args );

// The Loop
if ( $the_support_query->have_posts() ) {
	while ( $the_support_query->have_posts() ) {
		$the_support_query->the_post(); ?>


		<div class="cell">
			<div class="card">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'featured-medium' ); ?></a>
					<div class="card-section">
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<?php the_title('<h4>', '</h4>'); ?>
					</a>
				</div>
			  </div>
		</div>


	<?php
		// After 4 close the row div and open a new one
		if ( $i % 4 == 0 ) {
			echo '</div><div class="grid-x grid-padding-x small-up-1 large-up-3 large-offset-1">';
			}
	}
}
echo '</section>';
// Reset Post Data
wp_reset_postdata();

?>