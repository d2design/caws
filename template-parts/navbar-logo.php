<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 2/24/18
 * Time: 1:08 PM
 */


$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id, 'full' );
$url = home_url();
		if ( has_custom_logo() ) {
			echo '<a href="' . esc_url( $url ) . '">';
			echo '<img src="' . esc_url( $logo[0] ) . '">';
			echo '</a>';
		} else {
			echo '<a href="' . esc_url( $url ) . '">';
			echo '<h4>' . get_bloginfo( 'name' ) . '</h4>';
			echo '</a>';
		}
